
document.addEventListener('DOMContentLoaded', () => {
    let errorMessage = document.createElement('span')
    errorMessage.style.color = 'red'

    let inputs = document.querySelectorAll('input')
    inputs = Array.from(inputs)

    inputs = inputs.map((input) => {
        return input.addEventListener('change', () => {
            input.style.border = '1px solid rgba(0, 0, 0, 0.4)'

            if (input.nextElementSibling != null) {
                if (input.nextElementSibling.nodeName == 'SPAN') {
                    input.nextElementSibling.remove()
                }

            }
        })
    })

    document.addEventListener('submit', (event) => {
        event.preventDefault()
    
        let isFormValid = true


        const firstName = document.getElementById('firstName')

        if (nameNotValid(firstName.value)) {
            notValidStyling(firstName)
            if (firstName.nextElementSibling == null) {
                firstName.parentElement.append(errorMessage.cloneNode(true))
            }

            isFormValid = false
        }else{
            validStyling(firstName)
        }
        const lastName = document.getElementById('lastName')

        if (nameNotValid(lastName.value)) {

            notValidStyling(lastName)
            if (lastName.nextElementSibling == null) {
                lastName.parentElement.append(errorMessage.cloneNode(true))
            }

            isFormValid = false
        }else{
            validStyling(lastName)
        }

        const email = document.getElementById('email')
        if (!validEmail(email.value)) {
            notValidStyling(email)
            if (email.nextElementSibling == null) {
                email.parentElement.append(errorMessage.cloneNode(true))
            }


            isFormValid = false
        }else{
            validStyling(email)
        }

        const password = document.querySelector('input[name="Password"]')
        const confirmPassword = document.querySelector('input[name="Confirm password"]')

        if (!passwordCriteriaCheck(String(password.value))) {
            notValidStyling(password)
            if (password.nextElementSibling == null) {
                password.parentElement.append(errorMessage.cloneNode(true))
            }

            isFormValid=false
        }else{
            validStyling(password)
        }

        if (!passwordMatch(password.value, confirmPassword.value)) {
            notValidStyling(confirmPassword)

            if (confirmPassword.nextElementSibling == null) {
                confirmPassword.parentElement.append(errorMessage.cloneNode(true))
            }

            isFormValid = false
        }

        const checkbox = document.getElementById("terms")
        const terms = document.querySelector('.terms')

        if (!checkbox.checked) {
            errorMessage.textContent = "Please agree to our terms"
            if (terms.nextElementSibling.nodeName !== 'SPAN') {

                terms.insertAdjacentElement('afterend', errorMessage.cloneNode(true))
            }
            isFormValid = false
        }
        if (isFormValid) {
            let container = document.querySelector('.container')
            container.style.display = 'none'

            let successMessage=document.createElement('h2')
            successMessage.textContent="Congratulations! Your Account has been Created"
            successMessage.style.textAlign='center'
            successMessage.style.color='White'
            document.body.append(successMessage)

        }


    })

    function nameNotValid(name) {
        if (name.length == 0) {
            errorMessage.textContent = 'Name cannot be empty'
            return true
        } else if (!isNaN(name)) {
            errorMessage.textContent = 'Name cannot be a number'

            return true
        } else if (!name.match(/^[a-zA-Z'\s]+$/)) {
            errorMessage.textContent = 'Name cannot contain any special characters'

            return true
        } else {

            return false
        }
    }

    function validEmail(email) {
        let emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (emailRegEx.test(email)) {
            return true
        } else {
            errorMessage.textContent = 'Please enter a valid email!'
            return false
        }
    }

    function passwordMatch(password, confirmPassword) {
        if (password === confirmPassword) {
            return true
        } else {
            errorMessage.textContent = 'There is a password mismatch! Try again?'
            return false
        }
    }
    function passwordCriteriaCheck(password) {
        if (password.length < 8) {
            errorMessage.textContent = "Choose any password with length of atleast 8 characters!"

            return false
        } else {
            return true
        }

    }

    function notValidStyling(div) {
        div.style.border = '2px solid red'
    }

    function validStyling(div){
        div.style.border ='2px solid green'
    }
})
