// document.addEventListener("DOMContentLoaded", () => {
// })
const container = document.querySelector(".container")

fetch("https://fakestoreapi.com/users")
    .then((response) => {
        document.getElementById("loader").style.display = 'none'
        if (response.ok) {
            return response.json()
        } else if (response.status === 404) {
            throw new Error("Not Found")
        } else {
            throw new Error("Something went bad!")
        }
    })
    .then((data) => {
        try {

            data.forEach(user => {
                document.getElementById("loader").style.display = 'none'

                let item = document.createElement('div')
                item.setAttribute('class', 'item')

                let name = document.createElement('div')
                name.setAttribute('class', 'name')

                let fullName = document.createElement('h4')
                fullName.textContent = `${user.name.firstname} ${user.name.lastname}\u00A0`

                let userName = document.createElement('span')
                userName.textContent = `@${user.username}`

                let email = document.createElement('p')
                let emailIcon = document.createElement('i')
                emailIcon.setAttribute('class', 'fa-solid fa-envelope')
                email.append(emailIcon, ` ${user.email}`)

                let contact = document.createElement('p')
                let contactIcon = document.createElement('i')
                contactIcon.setAttribute('class', 'fa-solid fa-phone')
                contact.append(contactIcon, " " + user.phone)

                let address = document.createElement('address')
                let addressIcon = document.createElement('i')
                addressIcon.setAttribute('class', 'fas fa-home')
                address.append(addressIcon, ` ${user.address.number}, ${user.address.street}, ${user.address.city}, ${user.address.zipcode}`)

                name.append(fullName, userName)
                item.append(name, email, contact, address)
                container.append(item)

            })
        } catch (err) {
            document.body.append("Some Internal Error Occured")
            console.log(err)
        }
    })
    .catch((err) => {
        let errorMessage = document.createElement('h2')
        errorMessage.textContent = err

        document.body.append(errorMessage)
    })