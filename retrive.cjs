document.addEventListener("DOMContentLoaded", () => {

    const container = document.querySelector(".container")
    let numberOfProducts = 20

    for (let index = 0; index < numberOfProducts; index++) {

        fetch(`https://fakestoreapi.com/products/${index + 1}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                document.getElementById("loader").style.display = 'none'

                let item = document.createElement('div')
                item.setAttribute('class', 'item')

                let preview = document.createElement('img')
                preview.src = data.image

                let title = document.createElement('h3')
                title.innerText = data.title

                let description = document.createElement('p')
                description.innerText = data.description

                let price = document.createElement('h4')
                price.setAttribute('class', 'price')
                price.innerText = `$${data.price}`

                let rating = document.createElement('div')
                rating.setAttribute('class', 'rating')
                rating.innerHTML = `<span><i class="fa-solid fa-star"></i></span><strong>${data.rating.rate}</strong> based on ${data.rating.count} reviews`

                container.append(item)
                item.appendChild(preview)
                item.appendChild(title)
                item.appendChild(description)
                item.appendChild(price)
                item.appendChild(rating)
            })
            .catch((err) => {
                let errorMessage = document.createElement('h2')
                errorMessage.innerText = 'There Seems to be a problem! Please try again in few minutes'
                document.append(errorMessage)

                throw new Error('Error:', err)

            })
    }

})